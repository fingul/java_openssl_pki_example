# build

    mvn package

# openssl and java compatible pki

    #create private key
    openssl genrsa -out private.pem

    #create public key
    openssl rsa -pubout -in private.pem -out public.pem

    # convert private Key to PKCS#8 format (so Java can read it)
    openssl pkcs8 -topk8 -inform PEM -outform DER -in private.pem -out private.der -nocrypt

    # output public key portion in DER format (so Java can read it)
    openssl rsa -in private.pem -pubout -outform DER -out public.der

# create test resource

    mkdir -p src/test/resources
    cd src/test/resources
    openssl genrsa -out private.pem
    openssl rsa -pubout -in private.pem -out public.pem
    openssl pkcs8 -topk8 -inform PEM -outform DER -in private.pem -out private.der -nocrypt
    openssl rsa -in private.pem -pubout -outform DER -out public.der


# sample code

    crypt_test.AppTest.testApp