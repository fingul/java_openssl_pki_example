package crypt_test;

import org.apache.commons.io.FileUtils;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import java.io.File;
import java.security.PrivateKey;
import java.security.PublicKey;

import static crypt_test.App.*;

/**
 * Unit test for simple App.
 */
public class AppTest 
    extends TestCase
{
    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public AppTest( String testName )
    {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite()
    {
        return new TestSuite( AppTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp()
    {

        try {

            PublicKey publicKey = getPublicKey("src/test/resources/public.der");

            PrivateKey privateKey = getPrivateKey("src/test/resources/private.der");

            String in = "0123456789";

            String ciperText = encrypt(in, publicKey);

            String out = decrypt(ciperText, privateKey);

            assertEquals(in,out);

        } catch (Exception e) {
            assertTrue(false);
        }

    }
}
